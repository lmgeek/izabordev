<?php
/**
 * Created by PhpStorm.
 * User: lamnav
 * Date: 9/9/2018
 * Time: 00:59
 */

if ( $product->extra_id == null ){
//    $extras = Order\Extras::all();
} else {
    $extras = Order\Extras::where('id', '=', $product->extra_id)->firstOrFail();
}
$categories = Order\Category::all();
?>


<?php echo $__env->make('headerFront', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div style="position: relative; overflow: hidden; margin-bottom: 20px">
    <div class="progressive-image_container" style="position: absolute; width: 100%; height: 100%;">
        <img src="<?php echo e(asset('images/bg_header.png')); ?>"
             style="width: 100%; height: 100%; transition: -webkit-filter 1.5s ease 0s;">
    </div>
    <div style="position: relative;">
        <section class="ui container _1aP3Hvrg71IsrTM22x8W6m" style="height:20.5rem!important;"><h1
                class="ch2Gspxs_6Su97s_BWokH"><?php echo e(ucwords($product->name)); ?></h1>
        </section>
    </div>
</div>




























<div class="container">
    <div class="row">
        <div class="col-md-3 categories" >
            <h4>Categorias</h4>
            <ul>
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="/category/<?php echo e($category->id); ?>"><?php echo e($category->name); ?></a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <div class="col-md-6 product">
            
            
            <?php echo Form::open(['route' => 'cart.store']); ?>

            <?php echo csrf_field(); ?>

            <?php if(!empty( Voyager::image( $product->image ) )): ?>
                <img src="<?php echo e(Voyager::image( $product->image )); ?>" style="width:100%">
            <?php else: ?>
                <img src="<?php echo e(asset( 'images/no-image.png' )); ?>" style="width:100%">
            <?php endif; ?>
            
            <input type="hidden" name="id" value="<?php echo e($product->id); ?>">
            <input type="hidden" name="name" value="<?php echo e($product->name); ?>">
            <input type="hidden" name="price" value="<?php echo e($product->price); ?>">
            <input type="hidden" name="user_id" value="1">
            <div class="info">
                <h3><?php echo e(ucwords($product->name)); ?></h3>
                <p class="description"><?php echo $product->description; ?></p>
            </div>
            <div class="price">
                <h3>$ <?php echo e($product->price); ?></h3>
            </div>
            <hr>
            <h4>Extras</h4>
            <ul>
                <?php if( !empty($extras) ): ?>

                    
                    <li>
                        <input type="checkbox" name="extras" value="<?php echo e($extras->id); ?>"> <label for=""><?php echo e($extras->name); ?></label>
                        ( <small>+ $<?php echo e($extras->price); ?></small> )
                    </li>
                    
                <?php else: ?>
                    <li>
                        No contiene extras
                    </li>
                <?php endif; ?>
            </ul>
            <h4>Aclaraciones Adicionales</h4>
            <div class="form-group">
                <textarea class="form-control rounded-0" name="adicional" id="adicionales" rows="2"></textarea>
            </div>
            <hr>
            <div style="float: right;">
                <label for="">Cantidad</label>
                <input type="text" class="form-control" name="qty" min="1" value="1" max="9" style="max-width: 20%;display: initial;" pattern="[1-9]" required>
                <button class="btn btn-danger"> <i class="fas fa-cart-plus"></i> Agregar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>


<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
