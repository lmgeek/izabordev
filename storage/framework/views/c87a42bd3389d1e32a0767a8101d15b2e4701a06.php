<?php
/**
 * Created by PhpStorm.
 * User: lamnav
 * Date: 25/9/2018
 * Time: 17:10
 */
?>
<?php echo $__env->make('headerFront', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<style>
    .description {
        font-size: 12px;
        text-align: justify;
    }
</style>


<div style="position: relative; overflow: hidden; margin-bottom: 20px">
    <div class="progressive-image_container" style="position: absolute; width: 100%; height: 100%;">
        <img src="<?php echo e(asset('images/bg_header.png')); ?>"
             style="width: 100%; height: 100%; transition: -webkit-filter 1.5s ease 0s;">
    </div>
    <div style="position: relative;">
        <section class="ui container _1aP3Hvrg71IsrTM22x8W6m" style="height:20.5rem!important;"><h1
                class="ch2Gspxs_6Su97s_BWokH">Checkout - Detalles de compra</h1>
        </section>
    </div>
</div>




<div class="container">

    <?php if(session()->has('success_message')): ?>
        <div class="spacer"></div>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php endif; ?>

    <?php if(count($errors) > 0): ?>
        <div class="spacer"></div>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="">
        <div class="container" style="margin-bottom: 50px">
            <div class="row">
                <div class="col-md-6">

                    <?php if(Auth::guest()): ?>
                        <h2 class="text-center">Ya soy cliente</h2>
                        <hr>
                        <div class="col-md-6" style="padding-left: 0!important; padding-right: 0!important">
                            <form action="/">
                                

                                <a class="btn btn-block btn-social btn-google" style="color: #ffffff">
                                    <span class="fab fa-google"></span> Login con Google
                                </a>
                            </form>
                        </div>
                        <div class="col-md-6" style="padding-left: 0!important; padding-right: 0!important">
                            <form action="/">
                                
                                <a class="btn btn-block btn-social btn-facebook" style="color: #ffffff">
                                    <span class="fab fa-facebook"></span> Login con Facebook
                                </a>
                            </form>
                        </div>
                        <form action="<?php echo e(route('login')); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group form-group-default" id="emailGroup">
                                <label><?php echo e(__('voyager::generic.email')); ?></label>
                                <div class="controls">
                                    <input type="text" name="email" id="email" value="<?php echo e(old('email')); ?>"
                                           placeholder="<?php echo e(__('voyager::generic.email')); ?>" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group form-group-default" id="passwordGroup">
                                <label><?php echo e(__('voyager::generic.password')); ?></label>
                                <div class="controls">
                                    <input type="password" name="password" placeholder="<?php echo e(__('voyager::generic.password')); ?>"
                                           class="form-control" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block login-button" style="margin-right: 20px; float: left; width: 48%; color: #ffffff">
                                <span class="signingin hidden">
                                    <span class="voyager-refresh"></span> <?php echo e(__('voyager::login.registring')); ?>...</span>
                                <span class="signin"><?php echo e(__('voyager::generic.login')); ?></span>
                            </button>
                            <button type="button" class="btn btn-block login-button" data-toggle="modal" data-target="#myModal" style="float: left; width: 48%; color: #ffffff">
                                <span class="signin"><?php echo e(__('general.registry')); ?></span>
                            </button>
                        </form>

                        <?php if(session()->has('success_message')): ?>
                            <div class="alert alert-green">
                                <ul class="list-unstyled">
                                    <li><?php echo e(session()->get('success_message')); ?></li>
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <h2>Detalles de envío</h2>
                        <form action="<?php echo e(route('checkout.store')); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" value="<?php echo e(auth()->user()->email); ?>"
                                       placeholder="Introduce tu email">
                            </div>
                            <div class="form-group">
                                <label for="name">Nombre y Apellido</label>
                                <input type="text" class="form-control" name="name" id="name" value="<?php echo e(auth()->user()->name); ?>"
                                       placeholder="Introduce tu nombre">
                            </div>
                            <div class="form-group">
                                <label for="telefono">Teléfono</label>
                                <input type="text" class="form-control" maxlength="11" data-mask="(000) 0000-0000" name="phone" id="telefono" value="<?php echo e(auth()->user()->phone); ?>"
                                       placeholder="Introduce tu teléfono" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Dirección de entrega</label>
                                <textarea type="text" class="form-control" name="address" id="address"
                                          placeholder="Introduce tu dirección" required></textarea>
                            </div>
                            <div style="float: right;">
                                <a href="/" class="btn btn-primary" style="margin-right: 20px">
                                    <i class="fas fa-shopping-basket"></i> Seguir comprando
                                </a>
                                <button type="submit" class="btn btn-danger">
                                    <i class="fas fa-shopping-cart" style="font-size: 1em"></i> Completar pedido
                                </button>
                            </div>
                        </form>

                    <?php endif; ?>

                </div>
                <div class="col-md-6">
                    <h2 class="text-center">Resumen del pedido</h2>
                    <hr>
                    <table class='table table-hover table-responsive'>
                        <thead>
                        <tr>
                            <th class='thead' width="15%"></th>
                            <th class='textAlignLeft thead'></th>
                            <th class="thead" style="float: right">Cantidad</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                                <td class="text-center">

                                    <?php if(!empty( Voyager::image( $item->model->image ) )): ?>
                                        <img src="<?php echo e(Voyager::image( $item->model->image )); ?>" >
                                    <?php else: ?>
                                        <img src="<?php echo e(asset( 'images/no-image.png' )); ?>" >
                                    <?php endif; ?>

                                </td>
                                <td>
                                    <strong><?php echo e($item->model->name); ?></strong>
                                    <br>
                                    <p class="description"><?php echo e(ucwords($item->model->description)); ?></p>
                                    <p>&#36;<?php echo e($item->model->price); ?></p>
                                </td>
                                <td >
                                    <div class='input-group' style="float: right">

                                        <input type="text" name="quantity[1]" id="quantity"
                                               class="form-control input-number text-center"
                                               value="<?php echo e($item->qty); ?>" min="1"
                                               max="10" style="width: 50px">
                                    </div>
                                </td>
                            </tr>


                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <tr class="total" style="text-align: right;">
                            <td></td>
                            <td>
                                <b>Subtotal:</b><br>
                                <b>Tax (13%):</b><br>
                                <b>Costo envío:</b><br>
                                <b>Total:</b>
                            </td>
                            <td>
                                <?php
                                $subtotal = str_replace(',', '.', Cart::total());
                                $ShippingCost = config('cart.ShippingCost');
                                $total = $subtotal + $ShippingCost;
                                ?>
                                &#36;<?php echo e(Cart::subtotal()); ?><br>
                                &#36;<?php echo e(Cart::tax()); ?><br>
                                &#36;<?php echo e($ShippingCost); ?><br>
                                &#36;<?php echo e(str_replace('.', ',', $total)); ?>

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
