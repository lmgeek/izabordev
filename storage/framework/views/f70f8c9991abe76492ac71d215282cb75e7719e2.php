<?php echo $__env->make('headerFront', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style>
    body {
        background-image: url('<?php echo e(asset("images/0W8A1835.jpg")); ?>');
        background-color: <?php echo e(Voyager::setting("admin.bg_color", "#FFFFFF" )); ?>;
    }

    @media (max-width: 767px) {
        body.login .login-sidebar {
            border-top: 0px !important;
            border-left: 5px solid<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
        }
    }

    body.login .form-group-default.focused {
        border-color: <?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
    }

</style>

</head>
<body class="login">


<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class=" col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">

                        <div class="copy animated fadeIn text-center">
                            <h1>¿Qué estás buscando?</h1>
                            <p>Pide tu delivery ahora mismo</p>
                            <div class="form-group form-group-default" id="searchGroup" style="width: 80%;">
                                <div class="controls">
                                    <input type="text" name="search" id="search" value=""
                                           placeholder="<?php echo e(__('general.search_ppal')); ?>" width="50%"
                                           class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block search-button">
                                <span class="search">Buscar</span>
                            </button>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>


    </div> <!-- .row -->
</div> <!-- .container-fluid -->
<div>


    
    <div class="container">
        <br><br>
        <div class="row">
            <h1 class="text-center">Sugerendias del día</h1>
            <hr>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="products_box col-md-4">
                    <a href="/plato/<?php echo e($product->id); ?>">
                        <div class="image_product">
                            
                            <?php if(!empty( Voyager::image( $product->image ) )): ?>
                                <img src="<?php echo e(Voyager::image( $product->image )); ?>">
                            <?php else: ?>
                                <img src="<?php echo e(asset( 'images/no-image.png' )); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="content-info">
                            <span class="nameProduct"><?php echo e(ucwords($product->name)); ?></span>
                            <span class="descProduct"  data-toggle="tooltip" data-placement="top" title="<?php echo e($product->description); ?>"><?php echo e($product->description); ?></span>
                            <span class="priceProduct">$RD <?php echo e($product->price); ?></span>
                            <div class="ec-stars-wrapper">
                                <a href="#" data-value="1" title="Votar con 1 estrellas">&#9733;</a>
                                <a href="#" data-value="2" title="Votar con 2 estrellas">&#9733;</a>
                                <a href="#" data-value="3" title="Votar con 3 estrellas">&#9733;</a>
                                <a href="#" data-value="4" title="Votar con 4 estrellas">&#9733;</a>
                                <a href="#" data-value="5" title="Votar con 5 estrellas">&#9733;</a>
                            </div>
                            <noscript>Necesitas tener habilitado javascript para poder votar</noscript>
                        </div>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <hr>
    

</div>


<div class="container">
    <div class="row" style="padding: 70px 0px">

    </div>
</div>


<section class="content_module new_row apps show">
    <div class="new_row content_bg">
        <article class="new_center_content">
            <div class="content_info">
                <h2>Descarga iZabor en tu celular</h2>
                <h3>Opción que complace su paladar</h3>
                <ul>
                    <li>
                        <a href="https://itunes.apple.com"
                           target="_blank">
                            <img src="<?php echo e(asset('images/mob-button-appstore.png')); ?>" alt="Disponible en el App Store">
                        </a>
                    </li>
                    <li>
                        <a href="https://play.google.com/store/apps/developer?id=Luis+Armando+Mar%C3%ADn"
                           target="_blank">
                            <img src="<?php echo e(asset('images/mob-button-gplay.png')); ?>" alt="Disponible en Google Play">
                        </a>
                    </li>
                </ul>
            </div>
            <img src="<?php echo e(asset('images/mobile-app.png')); ?>" class="phone_img" alt="Descargar app">
        </article>
        <article class="new_row apps app_mobile">
            <h2>Para una mejor experiencia descargá la app</h2>

            <a href="https://play.google.com/store/apps/developer?id=Luis+Armando+Mar%C3%ADn" class="ghost es android" alt="" title="Disponible en Google Play">Disponible en Google Play</a>
        </article>
    </div>
</section>


<!-- Modal Login -->

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><?php echo e(__('voyager::login.signin_below')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="social-buttons">
                    <a class="btn btn-block btn-social btn-facebook">
                        <span class="fab fa-facebook"></span> Continuar con Facebook
                    </a>
                    <a class="btn btn-block btn-social btn-google">
                        <span class="fab fa-google"></span> Continuar con Google
                    </a>
                </div>
                <div class="line line-top text-center" style="margin: 2em auto;">
                    <small id="smallOr" class="sub">O registrate con tu email</small>
                    <hr class="hr">
                </div>
                <form action="<?php echo e(route('register')); ?>" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group form-group-default" id="namelGroup">
                        <label><?php echo e(__('voyager::generic.email')); ?></label>
                        <div class="controls">
                            <input type="text" name="email" id="email" value=""
                                   placeholder="<?php echo e(__('voyager::generic.email')); ?>" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label><?php echo e(__('voyager::generic.password')); ?></label>
                        <div class="controls">
                            <input type="password" name="password" placeholder="<?php echo e(__('voyager::generic.password')); ?>"
                                   class="form-control" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block login-button" style="margin-right: 20px">
                        <span class="signingin hidden"><span
                                class="voyager-refresh"></span> <?php echo e(__('voyager::login.registring')); ?>...</span>
                        <span class="signin"><?php echo e(__('voyager::generic.login')); ?></span>
                    </button>

                </form>
            </div>
            <div class="modal-footer"></div>
        </div>
        <?php if(session()->has('success_message')): ?>
            <div class="alert alert-green">
                <ul class="list-unstyled">
                    <li><?php echo e(session()->get('success_message')); ?></li>
                </ul>
            </div>
        <?php endif; ?>

        <?php if(session()->has('error_message')): ?>
            <div class="alert alert-red">
                <ul class="list-unstyled">
                    <li><?php echo e(session()->get('error_message')); ?></li>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- Modal Login -->


<!-- Modal Register -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Registrate</h4>
            </div>
            <div class="modal-body">
                <div class="social-buttons">
                    <a class="btn btn-block btn-social btn-facebook" >
                        <span class="fab fa-facebook"></span> Continuar con Facebook
                    </a>
                    <a class="btn btn-block btn-social btn-google" >
                        <span class="fab fa-google"></span> Continuar con Google
                    </a>
                </div>
                <div class="line line-top text-center" style="margin: 2em auto;">
                    <small id="smallOr" class="sub">O registrate con tu email</small>
                    <hr class="hr">
                </div>
                <form action="<?php echo e(route('register')); ?>" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group form-group-default" id="namelGroup">
                        <label><?php echo e(__('voyager::generic.name')); ?></label>
                        <div class="controls">
                            <input type="text" name="name" id="name" value=""
                                   placeholder="<?php echo e(__('voyager::generic.name')); ?>" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="emailGroup">
                        <label><?php echo e(__('voyager::generic.email')); ?></label>
                        <div class="controls">
                            <input type="text" name="email" id="email" value=""
                                   placeholder="<?php echo e(__('voyager::generic.email')); ?>" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label><?php echo e(__('voyager::generic.password')); ?></label>
                        <div class="controls">
                            <input type="password" name="password" placeholder="<?php echo e(__('voyager::generic.password')); ?>"
                                   class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label><?php echo e(__('general.password-confirma')); ?></label>
                        <div class="controls">
                            <input type="password" name="password_confirmation" id="password-confirm" placeholder="<?php echo e(__('general.password-confirm')); ?>"
                                   class="form-control" required>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-block login-button" style="width:100%">
                        <span class="signingin hidden"><span
                                class="voyager-refresh"></span> <?php echo e(__('voyager::login.registring')); ?>...</span>
                        <span class="signin"><?php echo e(__('general.registry')); ?></span>
                    </button>

                </form>
            </div>
            <div class="modal-footer"></div>
        </div>

    </div>
</div>
<!-- Modal -->




    



<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<script>
    var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="email"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function (ev) {
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function (e) {
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function (e) {
        document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function (e) {
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function (e) {
        document.getElementById('passwordGroup').classList.remove("focused");
    });

</script>
</body>
</html>
