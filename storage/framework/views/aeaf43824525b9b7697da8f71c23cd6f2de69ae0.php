<?php
/**
 * Created by PhpStorm.
 * User: lamnav
 * Date: 10/9/2018
 * Time: 02:17
 */


<form action="{{ route('voyager.carts.destroy', $c->id) }}" method="post">
{{ method_field("DELETE") }}
{{ csrf_field() }}
<button type="submit"> delete</button>
</form>
